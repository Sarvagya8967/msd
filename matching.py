import cv2 as cv
import numpy as np
from glob import glob
from matplotlib import pyplot as plt
from PIL import Image


def single_img(input_file):
    img = Image.open(input_file)
    range = img.getextrema()
    if((range[0][0]==range[0][1]) and (range[1][0]==range[1][1]) and (range[2][0]==range[2][1])):
        return True
    else:
        return False


input_file = ''   ## Add you root path
template = cv.imread('',0) ##Add your template
# print(template.shape)
avg_temp = np.mean(template)

# c, w, h = template.shape[::-1]
w, h = template.shape[::-1]
# All the 6 methods for comparison in a list
# methods = ['cv.TM_CCOEFF', 'cv.TM_CCOEFF_NORMED', 'cv.TM_CCORR',
#             'cv.TM_CCORR_NORMED', 'cv.TM_SQDIFF', 'cv.TM_SQDIFF_NORMED']
method = 'cv.TM_CCOEFF_NORMED'
method = eval(method)
threshold = 0.8
result = []
img_res = []
# for meth in methods:
images = glob(input_file+'*.jpg')
count = 0
for image in images:
    # print(image)
    img = cv.imread(image,0)
    # c_i, w_i, h_i = img.shape[::-1]
    w_i, h_i = img.shape[::-1]
    # print(img.shape)
    # print(template.shape)
    if((w_i<=w) or (h_i<=h)):
        continue
    else:

        # Apply template Matching
        res = cv.matchTemplate(img,template,method)
        # print(res.shape)
        loc = np.where( res >= threshold)
        if(loc[0].shape[0]!=0):
            result.append(loc)
            img_res.append(image)
# print(len(result))
print(img_res)
# print(loc)
# min_val, max_val, min_loc, max_loc = cv.minMaxLoc(loc)
# # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
# if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
#     top_left = min_loc
# else:
#     top_left = max_loc
# bottom_right = (top_left[0] + w, top_left[1] + h)
# print((loc[0].shape[0]==0))
for i in range (len(result)):
    # img = cv.imread(img_res[i],0)
    img_rgb = cv.imread(img_res[i])
    img_rgb = cv.cvtColor(img_rgb, cv.COLOR_BGR2RGB)
    template = cv.cvtColor(template, cv.COLOR_BGR2RGB)
    loc = result[i]
    for pt in zip(*loc[::-1]):
        cv.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,255,0), 1)
    # cv.rectangle(img,top_left, bottom_right, 255, 2)
    plt.subplot(121),plt.imshow(template)
    plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(img_rgb)
    plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
    plt.suptitle(method)
    plt.show()
