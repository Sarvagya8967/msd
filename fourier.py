import cv2
import numpy as np
import matplotlib.pyplot as plt



img = cv2.imread('',0) ## Add your image


bin_fft = np.zeros_like(img)
f = np.fft.fft2(img)
fshift = np.fft.fftshift(f)
magnitude_spectrum = 20*np.log(np.abs(fshift))
average = np.mean(magnitude_spectrum)
bin_fft[(magnitude_spectrum>average)]=1
# thresh = (np.max(magnitude_spectrum))
thresh = 340
# print(magnitude_spectrum.shape)
bin_fft[(magnitude_spectrum>=thresh)]=255
# print(np.max(magnitude_spectrum))
# print(np.max((fshift)))
print(fshift[ np.where( np.abs(fshift) > 1 ) ])
# result = np.where(magnitude_spectrum == np.amax(magnitude_spectrum))
result = np.where(fshift == np.amax(fshift))
# print(result[0], result[1])

plt.subplot(121),plt.imshow(img,cmap='gray')
plt.title('Input Image'), plt.xticks([]), plt.yticks([])
# plt.subplot(122),plt.imshow(np.abs(fshift), cmap='gray')
plt.subplot(122),plt.imshow(np.abs(magnitude_spectrum), cmap='gray')
plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
plt.show()
