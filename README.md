This is a write up for the assignment that was given for machine learning role at Mad Street Den. The following are the observations:

1) Template matching seems to work for almost all images except for some because of the differneces in image colour, grdiesnt, orientation and even the quality (with noise)
  - Some of the images that didn't work was because the image was either tilted, or had noise or was the same colour throughout. To tackle them, Fourier analysis was used.

2) Fourier analysis seems to help deal with some of the issues faced.
  - When we did the fourier transform of an image that was of the same colour throughout and calculate the magnitude spectrum, we get a line that passes through an x value that corrsponds to the magnitude of the frequency.
  - This is followed by PIL Image analysis where we get the max and mix values in an image. If they are equal, then the image has only one colour.
  - When we match the fourier tranfirm of this image with the original, we get the same line (to get the full line, the colour value needs to be there for all pixels)
  - As you can see from the images (fourier_colour, fourier_one_colour, hist_one_colour, hist_two_colour and colour), we see a clear distinction between the frequencies of the image and this is used to distinguish between a normal image and the ourlier.

3) Regarding the orientation, we again use fourier transform (non_tilt and tilt images). When we do the transform, we see that the max aplitudes are also tilted by the same angle as the image. So to rectify that, we need to tilt the image by that angle

4) I have attached the code for fourier transform.

5) The matching.py code matches an input template to the images present in the root folder
